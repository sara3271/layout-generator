import React from "react";
import { Container, Row } from "reactstrap";
import { ColumnContainer, ColumnChild, StyledCol } from "../StyledComponents";

function Grid({ gridArray }) {
  return (
    <Container>
      {gridArray.map((item, rowIndex) => {
        let colArray = [];

        for (let i = 0; i < item; i++) {
          colArray = colArray.concat([""]);
        }

        return (
          <Row key={rowIndex.toString()}>
            {colArray.map((item, colIndex) => (
              <StyledCol key={colIndex.toString()}>
                <ColumnContainer
                  className={
                    rowIndex === 0 && colIndex === 0
                      ? "firstRowAndColumn"
                      : colIndex === 0
                      ? "firstColumn"
                      : rowIndex === 0
                      ? "firstRow"
                      : ""
                  }
                >
                  <ColumnChild />
                </ColumnContainer>
              </StyledCol>
            ))}
          </Row>
        );
      })}
    </Container>
  );
}

export default Grid;
