import React, { useRef, useEffect } from "react";
import { StyledInput } from "../StyledComponents";

function Input({ value, onChange, isValid }) {
  const myInput = useRef();

  useEffect(() => {
    myInput.current.focus();
  }, []);

  return (
    <StyledInput
      className={value && !isValid && "outline"}
      ref={myInput}
      placeholder={"Type your value..."}
      value={value}
      onChange={onChange}
    />
  );
}

export default Input;
