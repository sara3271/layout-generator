import styled from "styled-components";
import { Col } from "reactstrap";

export const MainContainer = styled.div`
  background-color: #282c34;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  text-align: center;
  align-items: center;
  padding: 1.5rem;
`;

export const HeaderContainer = styled.div`
  min-height: 9rem;
  max-height: 9rem;
`;

export const Title = styled.h1`
  color: white;
`;

export const Error = styled.p`
  color: red;
  font-size: 0.8rem;
  margin-top: 0.4rem;
`;

export const GridContainer = styled.div`
  width: 100%;
`;

export const StyledInput = styled.input`
  &.outline {
    outline: 0.1rem solid red;
  }

  padding: 0.4rem;
`;

export const StyledCol = styled(Col)`
  padding: 0;
`;

export const ColumnContainer = styled.div`
  &.firstRowAndColumn {
    border-top: 0.1rem solid #ccc;
    border-left: 0.1rem solid #ccc;
  }
  &.firstColumn {
    border-left: 0.1rem solid #ccc;
  }
  &.firstRow {
    border-top: 0.1rem solid #ccc;
  }

  width: 100%;
  height: 100%;
  min-height: 80;
  border-right: 0.1rem solid #ccc;
  border-bottom: 0.1rem solid #ccc;
`;

export const ColumnChild = styled.div`
  min-height: 3rem;
`;
