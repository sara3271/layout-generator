import React, { useState } from "react";
import Grid from "./components/Grid";
import Input from "./components/Input";
import {
  MainContainer,
  Title,
  HeaderContainer,
  Error,
  GridContainer,
} from "./StyledComponents";

function App() {
  const [value, setValue] = useState("");
  const [gridArray, setGridArray] = useState([]);
  const [isValid, setIsValid] = useState(false);

  function handleInputChange(e) {
    let regex = /^(([1-9][0-9]*)\/)*([1-9][0-9]*)$/;
    let inputValue = e.target.value;

    setIsValid(regex.test(inputValue));

    setValue(inputValue);
    let inputStrArray = inputValue.split("/");

    const inputNumberArray = inputStrArray.map((item) => {
      return Number(item);
    });

    setGridArray(inputNumberArray);
  }

  return (
    <MainContainer>
      <HeaderContainer>
        <Title> Layout Generator </Title>
        <Input value={value} onChange={handleInputChange} isValid={isValid} />

        {value && !isValid && <Error>Invalid Format!</Error>}
      </HeaderContainer>

      <GridContainer>{isValid && <Grid gridArray={gridArray} />}</GridContainer>
    </MainContainer>
  );
}

export default App;
